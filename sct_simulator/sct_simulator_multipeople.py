#!/usr/bin/env python3
# sct_simulator_multipeople.py
# implements fluid-tank model of human mutual encouragement.
# this uses a simplified model from the example originally
# presented on 9/1//2016 in class, using the version
# presented on 9/13. It has been modified to include two 
# humans in the model.
#
# Based on the model described in:
# "A decision framework for an adaptive behavioral intervention
# for physical activity using adaptive model predictive control"
# Martin et al.
# ACC 2016
#
# Adam Allevato, Dorothy Jorgensen
# ME 396D: Design/Control of Human-Centered Robots
# HW 1
# University of Texas
# Fall 2016
#######################################################

# modules
import numpy as np
import matplotlib.pyplot as plt
import cvxpy as cvx

#######################################################

t = 0
delta_t = 1
t_final = 200

# coefficient matrices
# how the state rate of change depends on the state
# "constant contact" model
# A = np.matrix([ [ -1, 0, 0, 0, 0, 0, 0.5, 0.5, 0, 0], # 2A
#                  [ 0, -1, 0, 0, 0, 0, 0.5, 0.5, 0, 0], # 2B

#                  [ 0, 0, -1, 0, 0.2, 0, 0, 0, 1, 1], # 3A
#                  [ 0, 0, 0, -1, 0, 0.2, 0, 0, 2, 2], # 3B

#                  [ 0.5, 0, 0.2, 0, -1, 0, 0.5, 0, 0, 0], # 4A
#                  [ 0, 0.5, 0, 0.2, 0, -1, 0, 0.5, 0, 0], # 4B

#                  [ 0, 0, 0, 0, 0.5, 0, -1, 0, 0, 0], # 5A
#                  [ 0, 0, 0, 0, 0, 0.5, 0, -1, 0, 0], # 5B

#                  [ 0, 0, 0, 0, 0, 0, 0, 0, -1, 0],  # 7A
#                  [ 0, 0, 0, 0, 0, 0, 0, 0, 0, -1] # 7B

#                   ] )

# self-sustaining model
A = np.matrix([ [ -1, 0, 0, 0, 0, 0, 0.5, 0.5, 0, 0], # 2A
                 [ 0, -1, 0, 0, 0, 0, 0.5, 0.5, 0, 0], # 2B

                 [ 0, 0, -0.25, 0, 0.2, 0, 0.75, 0, 1, 1], # 3A
                 [ 0, 0, 0, -0.25, 0, 0.2, 0, 0.75, 2, 2], # 3B

                 [ 0.5, 0, 0.2, 0, -1, 0, 0.5, 0, 0, 0], # 4A
                 [ 0, 0.5, 0, 0.2, 0, -1, 0, 0.5, 0, 0], # 4B

                 [ 0, 0, 0, 0, 0.5, 0, -1, 0, 0, 0], # 5A
                 [ 0, 0, 0, 0, 0, 0.5, 0, -1, 0, 0], # 5B

                 [ 0, 0, 0, 0, 0, 0, 0, 0, -1, 0],  # 7A
                 [ 0, 0, 0, 0, 0, 0, 0, 0, 0, -1] # 7B

                  ] )

# how the state depends on the control input
B = np.matrix([[0], 
                [0], 
                [0], 
                [0], 
                [0], 
                [0], 
                [0], 
                [0], 
                [1], 
                [1]
                ])

# history of x
# initial conditions are already built-in
x = np.ones(shape=(1,10)).transpose() * 5

# constant control input u
u = np.ones(shape=(1,1)) * 10

comm_interval = 0
last_comm = 0
comm_cutoff = 100
def control(t):
    global comm_interval
    global last_comm

    if(t-last_comm == comm_interval and t < comm_cutoff):
        last_comm = t
        comm_interval += 1
        return u

    return u * 0

# simulate the tank dynamics and calculate outputs.
def forward(state, ctrl):
    # uses first-order Taylor expansion of system dynamics,
    # discretizing the derivative in time and assuming
    # linear dynamics
    new_state = state + A*state*delta_t + B*ctrl;
    return new_state

# run simulation and plot results
def simulate():
    global t
    global x
    ts = [0]
    t = 0

    # step through time
    while t < t_final:
        # for some reason, we need to transpose the very first input.
        # indexing a matrix with only one column returns the results
        # in one row as a Python list, instead of a columnar vector
        # in numpy format
        if t == 0:
            v = forward(np.matrix(x[:,t]).transpose(), control(t))
        # normal behavior: add the 
        else:
            v = forward(np.matrix(x[:,t]), control(t))
        x = np.append(x,v, axis = 1)

        t = t + delta_t
        ts.append(t)

    print("length of t", len(ts), "shape of x", np.shape(x))

    # create a grid of subplots
    xsize = 2
    ysize = 5
    f, plotgrid = plt.subplots(ysize, xsize)

    # fix strange axis offsets added by matplotlib
    [[p.ticklabel_format(useOffset=False) for p in q] for q in plotgrid]

    # only add axis labels on bottom graph
    for x_step in range(0, xsize):
        plotgrid[ysize-1, x_step].set_xlabel('t, days')

    plotgrid[0,0].plot(ts, x[0,:].transpose())
    plotgrid[0,0].set_title('Self-expectation')
    plotgrid[0,0].set_ylabel("x_a[2]")

    plotgrid[1,0].plot(ts, x[2,:].transpose())
    plotgrid[1,0].set_title('Confidence')
    plotgrid[1,0].set_ylabel("x_a[3]")

    plotgrid[2,0].plot(ts, x[4,:].transpose())
    plotgrid[2,0].set_title('Behavior')
    plotgrid[2,0].set_ylabel("x_a[4]")

    plotgrid[3,0].plot(ts, x[6,:].transpose())
    plotgrid[3,0].set_title('Goal Achievement')
    plotgrid[3,0].set_ylabel("x_a[5]")

    plotgrid[4,0].plot(ts, x[8,:].transpose())
    plotgrid[4,0].set_title('Communication')
    plotgrid[4,0].set_ylabel("x_a[7]")

    plotgrid[0,1].plot(ts, x[1,:].transpose())
    plotgrid[0,1].set_title('Self-expectation')
    plotgrid[0,1].set_ylabel("x_b[2]")

    plotgrid[1,1].plot(ts, x[3,:].transpose())
    plotgrid[1,1].set_title('Confidence')
    plotgrid[1,1].set_ylabel("x_b[3]")

    plotgrid[2,1].plot(ts, x[5,:].transpose())
    plotgrid[2,1].set_title('Behavior')
    plotgrid[2,1].set_ylabel("x_b[4]")

    plotgrid[3,1].plot(ts, x[7,:].transpose())
    plotgrid[3,1].set_title('Goal Achievement')
    plotgrid[3,1].set_ylabel("x_b[5]")

    plotgrid[4,1].plot(ts, x[9,:].transpose())
    plotgrid[4,1].set_title('Communication')
    plotgrid[4,1].set_ylabel("x_b[7]")


    plt.show()

#######################################################

# program entry point
if __name__ == "__main__":
    print("SCT model simulating....")
    simulate()