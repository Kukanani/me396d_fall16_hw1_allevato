#!/usr/bin/env python3
# sct_simulator.py
# implements fluid-tank model of human motivation and behavior.
# this is a simplified model from the full example originally
# presented on 9/1/2016 in class, instead using the version
# presented on 9/13.
# 
# The control input is found using model-predictive control
# with a short time horizon, with solutions found by the CVXPY
# solver and re-calculated every time step.
#
# Model is based on:
# "A decision framework for an adaptive behavioral intervention
# for physical activity using adaptive model predictive control"
# Martin et al.
# ACC 2016
#
# Adam Allevato
# ME 396D: Design/Control of Human-Centered Robots
# HW 1
# University of Texas
# Fall 2016
#######################################################

# modules
import numpy as np
import matplotlib.pyplot as plt
import cvxpy as cvx

#######################################################

# first constraint set: tank dynamics, pg 3577

gamma = {29: 2.5, 311: 0.4, 57: 1, 510: 0.6, 64: 1.5, 68: 1.5}
beta = {25: 0.5, 34: 0.2, 42: 0.3, 43: 0.9, 46: 0.9, 45: 0.5, 54: 0.6}

tau = {2: 10, 3: 30, 4: 1.25, 5: 2, 6: 1.25}
#tau = {2: 10, 3: 30, 4: 0.8, 5: 2, 6: 0.5} # original inputs
# thetas and zetas are considered 0, so don't even put them into the model!

# time step variables
t = 0
ts = [0]
delta_t = 1
t_max = 160

# set up MPC formulation
m = 5
p = 7

# cost function multiplier matrices. Zero means that the given variable will not be used
# at all in calculating the cost.
Q_y = np.matrix([[1]])
Q_delta_u = np.zeros([6,6])
Q_u = np.zeros([6,6])

# desired values
y_desired = 10
u_desired = np.matrix([0,0,0]).transpose()

# solver constraints
u_min       = np.matrix([0, 0, 0]).transpose()
u_max       = np.matrix([15, 10, 10]).transpose()
delta_u_min = np.matrix([-1, -0.5, -0.5]).transpose()
delta_u_max = np.matrix([1, 0.5, 0.5]).transpose()
x_max       = np.matrix([100, 100, 12, 100, 100]).transpose()
x_min       = np.matrix([0, 0, 0, 0, 0]).transpose()

# initial condition on system state
init_eta = np.matrix([[0],[0],[5],[0],[0]])

u_act = np.matrix([[7],[0],[0]])
eta = {2: [init_eta[0,0]], 3: [init_eta[1,0]], 4: [init_eta[2,0]], 5: [init_eta[3,0]], 6: [init_eta[4,0]]}

# simplification: transfer function c_sr = 1
c_sr = 1

# coefficient matrices
# how the state depends on the state
Ax = np.matrix([ [ 1-delta_t/tau[2], 0,0,beta[25]*delta_t/tau[2],0],
                 [0, 1-delta_t/tau[3], (beta[34]+gamma[311])*delta_t/tau[3],0,0],
                 [beta[42]*delta_t/tau[4], beta[43]*delta_t/tau[4], 1-delta_t/tau[4], beta[45]*delta_t/tau[4], beta[46]*delta_t/tau[4]],
                 [0,0,beta[54]*delta_t/tau[5], 1-delta_t/tau[5], 0],
                 [0,0,-gamma[64]*delta_t*c_sr/tau[6],0, 1-delta_t/tau[6] ] ] )

# how the state depends on the inputs (u 8, 9, 10)
B1 = np.matrix( [[0, (gamma[29]/tau[2]*delta_t), 0],
                 [(gamma[311]/tau[3]*delta_t), 0, 0],
                 [0, 0, 0],
                 [0, 0, (gamma[510]/tau[5]*delta_t)],
                 [(gamma[64]/tau[6]*delta_t), 0, 0]])

# solve the MPC problem with a small time horizon, as defined above
def control_mpc():
    global u_act

    #set up CVXPY variables
    u = cvx.Variable(3,p+1)
    x = cvx.Variable(5,p+1)

    # build cost functions and constraints for each time step
    states = []
    for i in range(p):
        cost = 0
        constraints = [x[:, i+1] == (Ax*x[:, i] + B1*u[:,i])
                      ,u[:,i+1] <= u_max
                      ,u[:,i+1] >= u_min
                      ,x[:,i+1] <= x_max
                      ,x[:,i+1] >= x_min
                      ]
        if(i < m): # if in control horizon, additional constraints and costs on control input
            constraints += [u[:,i+1] - u[:,i] >= delta_u_min,
                            u[:,i+1] - u[:,i] <= delta_u_max
                           ]
            if(np.sum(Q_u) > 0 or np.sum(Q_delta_u) > 0):
                print("also minimizing u and delta u cost")
                cost += cvx.quad_form(u[:,i] - u_desired, Q_u) + cvx.quad_form(u[:,i+1] - u[:,i], Q_delta_u)
        
        cost += (x[2, i+1] - y_desired) ** 2
        states.append(cvx.Problem(cvx.Minimize(cost), constraints))

    # minimize sum of all cost functions, subject to all constraints
    mpc_problem = sum(states)

    # force initial conditions at start of MPC loop to current system conditions
    #print("initial u", u_act[:,-1])
    mpc_problem.constraints += [u[:,0] == u_act[:,-1]]
    mpc_problem.constraints += [x[:,0] == np.array([eta[2][-1], eta[3][-1], eta[4][-1], eta[5][-1], eta[6][-1]])]
    
    # attempt solution
    failed = False
    try:
        mpc_problem.solve()
    except:
        failed = True

    # check for problem completion
    if(failed or (mpc_problem.status is cvx.INFEASIBLE or mpc_problem.status is cvx.INFEASIBLE_INACCURATE)):
        print("Solver failed.")
        print("Status:", mpc_problem.status)
        return False

    # append the determined control input to the list of inputs
    u_act = np.append(u_act, [[u.value[0,1]], [u.value[1,1]], [u.value[2,1]]], axis=1)

    # print("optimal value", mpc_problem.value)
    # print("optimal u:", u.value)

    # we aren't using xsi anymore in this formulation. But if we were, here's how the values would be calculated
    # xsi[4].append(u.value[0,1]-eta[4][-1])
    # xsi[7].append(0)
    # xsi[8].append(0)
    # xsi[9].append(u.value[1,1])
    # xsi[10].append(u.value[2,1]) # = 0 if goal not set
    # xsi[11].append(eta[4][-1]-u.value[0,1])
    return True

# simulate the tank dynamics and calculate outputs. Straightforward dynamics equation
def forward(state, sys_input):
    new_state = Ax * state + B1 * sys_input;
    return new_state

def simulate():
    global t
    global ts
    global u_act

    shouldContinue = True
    while t < 160 and shouldContinue:
        shouldContinue = control_mpc()
        #print(u_act[:,-1])
        if(not shouldContinue):
            print("MPC stopped finding a solution at time t =", t)
            continue

        state = np.matrix([[eta[2][t], eta[3][t], eta[4][t], eta[5][t], eta[6][t]]]).transpose()
        new_state = forward(state, u_act[:,-1])

        eta[2].append(new_state[0,0])
        eta[3].append(new_state[1,0])
        eta[4].append(new_state[2,0])
        eta[5].append(new_state[3,0])
        eta[6].append(new_state[4,0])

        t = t + delta_t
        ts.append(t)

def plot():
    # PLOTTING

    # create a grid of suplots
    xsize = 2
    ysize = 3
    f, plotgrid = plt.subplots(ysize, xsize)

    # disable strange matplotlib offset
    [[p.ticklabel_format(useOffset=False) for p in q] for q in plotgrid]

    # only set labels on the bottom two graphs
    for x_step in range(0, xsize):
        plotgrid[ysize-1, x_step].set_xlabel('t, days')

    plotgrid[0,1].plot(ts, u_act[0,:].transpose())
    plotgrid[0,1].set_title('Step goal')
    plotgrid[0,1].set_ylabel("u[8]")

    plotgrid[1,1].plot(ts, u_act[1,:].transpose())
    plotgrid[1,1].set_title('Expected points')
    plotgrid[1,1].set_ylabel("u[9]")

    plotgrid[2,1].plot(ts, u_act[2,:].transpose())
    plotgrid[2,1].set_title('Awarded points')
    plotgrid[2,1].set_ylabel("u[10]")

    plotgrid[0,0].plot(ts, eta[2])
    plotgrid[0,0].set_title('Outcome expectancy')
    plotgrid[0,0].set_ylabel("eta[2]")

    plotgrid[1,0].plot(ts, eta[3])
    plotgrid[1,0].set_title('Self-efficacy')
    plotgrid[1,0].set_ylabel("eta[3]")

    plotgrid[2,0].plot(ts, eta[4])
    plotgrid[2,0].set_title('Behavior')
    plotgrid[2,0].set_ylabel("eta[4]")

    # done! Show the plot
    plt.show()

#######################################################

# program entry point
if __name__ == "__main__":
    print("SCT model simulating....")
    simulate()
    plot()